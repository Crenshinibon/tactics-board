class Player
    constructor: (pData, scenario, shift) ->
        
        

Template.players.rendered = () ->
    p = @findAll '.player'
    $(p).draggable()
    
    rh = @findAll 'rotation-handle'
    $(rh).draggable()

    
Template.players.helpers
    players: () ->
        players = []
        scenario = Scenarios.findOne {_id: 'default'}
        s1 = scenario.steps[0]
        players.push new Player s1.player1, scenario, s1
        players.push new Player s1.player2, scenario, s1
        players.push new Player s1.player3, scenario, s1
        players.push new Player s1.player4, scenario, s1
        players.push new Player s1.player5, scenario, s1
        players.push new Player s1.player6, scenario, s1
        players

Template.player.helpers
    playerOnPlayground: () ->
        false

    playerHeight: () ->
        20
        
    playerWidth: () ->
        25
        
    yOffset: () ->
        0
        
    xOffset: () ->
        0
    
    playerRotation: () ->
        0
    
    playerColor: () ->
        'red'
    playerBackgroundColor: () ->
        'black'
       