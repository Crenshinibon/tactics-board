Layout = {}
@Layout = Layout

_windowWidth = new ReactiveVar
_windowHeight = new ReactiveVar

_resizeListener = []

Layout.orientation = () ->
    if _windowWidth.get() > _windowHeight.get()
        'horizontal'
    else
        'vertical'
        
Layout.horizontal = () ->
    _windowWidth.get() > _windowHeight.get()

Layout.vertical = () ->
    not Layout.horizontal()
    
Layout.windowWidth = () ->
    _windowWidth.get()
    
Layout.windowHeight = () ->
    _windowHeight.get()
        
Layout.onResize = (e) ->
    _resizeListener.push e
    
Layout._layout = () ->
    _windowWidth.set window.innerWidth
    _windowHeight.set window.innerHeight
    _resizeListener.forEach (e) ->
        e.call @, _windowWidth.get(), _windowHeight.get()


Template.body.rendered = () ->
    Layout._layout()
    
    $(window).resize (e) ->
        Layout._layout()    
       