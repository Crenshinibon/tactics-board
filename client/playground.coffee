_xOffset = new ReactiveVar
_yOffset = new ReactiveVar
_width = new ReactiveVar
_height = new ReactiveVar

_dims = () ->
    "left: #{_xOffset.get()}, top: #{_yOffset.get()}, width: #{_width.get()}, height: #{_height.get()} "

_calcDimensions = (wWidth, wHeight) ->
    console.log "windowWidth:", wWidth, "windowHeight", wHeight
    #horizontal
    if wWidth > wHeight
        #full width
        if wHeight > (wWidth / 2)
            _xOffset.set 0
            _yOffset.set 0
            _height.set (wWidth / 2)
            _width.set wWidth
        else
            _xOffset.set (wWidth - wHeight * 2)
            _yOffset.set 0
            _height.set wHeight
            _width.set (wHeight * 2)
    #vertical
    else
        #full height
        if wWidth > (wHeight / 2)
            _xOffset.set (wWidth - (wHeight / 2))
            _yOffset.set 0
            _height.set wHeight
            _width.set (wHeight / 2)
        else
            _xOffset.set 0
            _yOffset.set 0
            _height.set (wWidth * 2)
            _width.set wWidth
            
    console.log _dims()


Template.playground.created = () ->
    Layout.onResize (wWidth, wHeight) ->
        _calcDimensions wWidth, wHeight

_freeZone = () ->
    if Layout.horizontal()
        _height.get() / 5
    else
        _width.get() / 5

_netWidth = () ->
    if Layout.horizontal()
        _width.get() / 100
    else
        _width.get() - (_freeZone() / 2)
        
_netHeight = () ->
    if Layout.vertical()
        _height.get() / 100
    else
        _height.get() - (_freeZone() / 2)
        
Template.playground.helpers
    xOffset: () ->
        "#{_xOffset.get()}px"
    yOffset: () ->
        "#{_yOffset.get()}px"
    width: () ->
        "#{_width.get()}px"
    height: () ->
        "#{_height.get()}px"
    horizontal: () ->
        Layout.horizontal()
        
    sideWidth: () ->
        w = 0
        if Layout.horizontal()
            w = _height.get() - _freeZone()
        else
            w = _width.get() - _freeZone()
        "#{w}px"
        
    sideHeight: () ->
        h = 0
        if Layout.vertical()
            h = _width.get() - _freeZone()
        else
            h = _height.get() - _freeZone()
        "#{h}px"
        
    opponentXOffset: () ->
        if Layout.horizontal()
            offSet = _xOffset.get() + (_width.get() / 2)
            "#{offSet}px"
        else
            "#{_xOffset.get() + (_freeZone() / 2)}px"
    
    opponentYOffset: () ->
        if Layout.horizontal()
            "#{_freeZone() / 2}px"
        else
            "#{_freeZone()}px"
        
    ownXOffset: () ->
        if Layout.horizontal()
            "#{_xOffset.get() + _freeZone()}px"
        else
            "#{_xOffset.get() + (_freeZone() / 2)}px"
    
    ownYOffset: () ->
        if Layout.horizontal()
            "#{_freeZone() / 2}px"
        else
            "#{_height.get() / 2}px"

    netWidth: () ->
        "#{_netWidth()}px"
    
    netHeight: () ->
        "#{_netHeight()}px"
        
    netXOffset: () ->
        if Layout.horizontal()
            offSet = _xOffset.get() + (_width.get() / 2)
            "#{offSet - (_netWidth() / 2)}px"
        else
            "#{_xOffset.get() + (_freeZone() / 4)}px"
    netYOffset: () ->
        if Layout.horizontal()
            "#{_freeZone() / 4}px"
        else
            "#{(_height.get() / 2) - (_netHeight() / 2)}px"
    
