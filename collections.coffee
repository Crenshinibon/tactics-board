@Scenarios = new Meteor.Collection 'scenario'

@Players = new Meteor.Collection 'players'

@Situations = ['serve', 'receive', 'attackLeft', 'attackMiddle', 'attackRight', 'defendLeft', 'defendMiddle', 'defendRight']

if Meteor.isServer
    defScenario = @Scenarios.findOne {_id: 'default'}
    unless defScenario?
        defScenario =
            _id: 'default'
            playerLabels:
                player1: 'Setter#1'
                player2: 'Setter#2'
                player3: 'MiddleBlock#1'
                player4: 'Attacker#1'
                player5: 'Attacker#2'
                player6: 'MiddleBlock#2'
            steps: [
                label: 'First Serve'
                situation: 'serve'
                rotation: 1
                player1:
                    pos: 1
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0
                player2:
                    pos: 2
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0 #looking direction
                player3:
                    pos: 3
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0
                player4:
                    pos: 4
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0
                player5:
                    pos: 5
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0
                player6:
                    pos: 6
                    posRelX: 1 #factor
                    posRelY: 1 #factor
                    dir: 0
            ]
        
        @Scenarios.insert defScenario
        
                